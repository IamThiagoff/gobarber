# GoStack

## Módulo 01 – Ambiente de Desenvolvimento

- Visual Studio Code
- Tema e fonte [Terminal]
- Oh My Zsh [Terminal]
- Extensões Chrome e Ferramentas

## Módulo 02 – Ambiente Node e Conceitos

- Instalando Node, NPM e Yarn
- Conceitos do Node.js e API REST
- Criando aplicação
- Query & Route params
- Utilizando Insomnia e o Nodemon
- CRUD
- Middlewares
- Debugando aplicação
